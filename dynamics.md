# Conformational Dynamics and Energy Landscapes of Ligand Binding in RyR1

**Q:What did they do?**

They present 

* quantitative free energy landscapes,
* reaction coordinates,
* and 3D movies
  
of the continous conformational changes associated with the binding of activating ligand.

before and after the introduction of ligands

**Q: What did they use?**

They used snapshots of RyR1.

**Q: What did they use to take the snapshots of RyR1?**

They used single-particle cryo-EM

**Q: What is RyR1?**

It is a Ca 2+ channel involved in skeletal muscle excitation/contraction coupling.

**Q: What did they find?**

They found multiple routes to ligand binding with comparable branching ratios

They found that all high-probability routes involve significant conformational changes before and after the binding of ligands

They found new insights about the local structural changes along the ligand binding route including the accomodations at the

* calcium  
* ATP
* caffeine

binding sites

**Q: What is branching ratio?**

**Q: What do they claim?**

They claim their observations shed new light on the mechanisms and conformational routes to ligand binding 

They claim their approach can be used to elucidate the conformational changes associated with function in a wide range of systems.

They claim their work establishes that single-particle structural study of systems in equilibrium offers powerful insights into biological processes hitherto thought accessible only via non-equilibrium, time-resolved experiments

They claim the plethora of discrete conformations revealed by powerful maximum likelihood classification methods provide scant guidance on the sequence of conformational changes involved in ligand binding and channel gating, the responsible reaction coordinates, the energy landscapes with and without ligands, or the routes to ligand binding and gating.

**Q: On what topic is there a debate?**

the mechanisms by which ligands bind to macromolecular substrates. Even the nature and sequence of events are controversial 

**Q: What are the ways to elucidate the mechanism?**

Experimentally determined multidimensional landscapes specifying the free energies of the conformations relevant to function offer a powerful framework for elucidating mechanisms, but have remained largely inaccessible

**Q:What method did they use to combine the snapshots?**

Using geometric machine learning techniques [15-17], we have previously demonstrated that experimental snapshots of single molecular machines idling in equilibrium can be used to determine their energy landscapes in terms of orthogonal coordinates describing continuous conformational changes relevant to translation [17]

Such landscapes reveal all conformations with energies up to an upper limit set by the vanishing occupation probability of high-energy states. 

The key point is that thermal fluctuations in equilibrium lead to sightings of all states up to the limit set by the number of snapshots in the dataset (To they mean the number of snaphots sets the upper limit? If so isn't the upper limit only dependent on the temperature and not the number of snapshots)

**Q: What are their key changes to their methods?**

A key feature of the present study is the pooling of cryo-EM snapshots from two experiments.

In one, RyR1 macromolecules were in equilibrium with a thermal bath. In the other, the macromolecules were in equilibrium with a thermal bath and a ligand reservoir [12]

**Q: What are the improvements of their method?**

This pooling of data has two important consequences

* both species (with and without ligands, henceforth ±ligand) are described in terms of the same set of conformational reaction coordinates
* the approach reveals the heavily populated conformational conduits connecting the two species, thus identifying the lowest-energy routes relevant to ligand binding

**Q: What does this approach allow us to do?**

The approach allows us to determine the ±ligand energy landscapes for RyR1 in terms of the same binding-relevant reaction coordinates

**Q: How is the transition probability between two landscapes estimated?**

Fermi’s Golden Rule [18, 19] Supplementary section 3) is then used to estimate the transition probability between the two landscapes, with “hotspots” identifying the most probable transition points between the landscapes

**Q: How did they created the movies?**

Using the minimum-energy conduits(path?) on each landscape, we are thus able to compile, along high- probability routes, three-dimensional (3D) movies of ligand binding revealing changes in the overall channel conformation and at key binding sites

**Q: How did their results obtained using multiple landscape differe from the method that only uses one landscape?**

Our results demonstrate that the course of these continuous conformational changes differs in important ways from that obtained by morphing between discrete conformations [12]

**Q: How many snapshots of RyR1 did they use?**

The 791,956 cryo-EM snapshots of RyR1 molecules analyzed in this study comprised of about the same number of molecules in equilibrium with reservoirs with and without ligands (Ca2+, ATP, and caffeine) prior to cryo-freezing [12]

These snapshots were grouped into 1,117 uniformly spaced orientational classes by standard procedures [20]

**Q: How did they find the reaction coordinates?**

Geometric (manifold-based) analysis [17, 21] of the pooled dataset revealed at least four significant orthogonal conformational (reaction) coordinates, each describing a concerted set of continuous changes

**Q: What does the conformational changes along the first two reaction coordinate represent?**

* ALong RC1 involve the shell and pore
* Along RC2 involve the activation core

**Q: What does Fig1 show?**

1A: shows the ±ligand energy landscapes of RyR1

Assuming the probability of a collision (not binding) with a ligand is independent of conformation, the probability of a transition between equivalent points on the two landscapes can be estimated from Fermi’s Golden Rule for the period immediately after the exposure of RyR1 molecules to the reservoir containing ligands

$P(Collision|C) = P(Collision)$

$P(A<->B|C)$ : Transition probablity between bound and unbount state given a configuration

$P(A<->B, C) = P(A<->B|C)P(C)$

$P(A<->B, C)$ : probablity of transition between equivalent points on the two landscapes.

$A$ : Ligand not bound

$B$ : Ligand bound state

$C$ : The configuration of molecule

$<->$ : transitions between two states in both ways



1B: Inter-landscape transition probablity

the inter- landscape transition probability displays specific “hotspot” regions, where a significant number of ligand-free and ligand-bound macromolecules display the same conformation

Does this mean that the bound configurations also exist in the unbound configurations and has a high transtions probablity. Thus it indicates of a selective binding mechanism rather than an induced fit.

The mechanism:

The most probable routes to ligand binding start from the region of lowest energy on the –ligand landscape (“START” in Fig. 1(A)), reach one of the hotspot transition points (“HOT”) with a probability of ~2%, cross to the +ligand landscape also with ~ 0.45% of the probability of a collision with a ligand, and terminate in the region of lowest energy on the +ligand landscape (“FINISH”). This means ~ 0.01% of collisions with a ligand lead to binding



**most probable route:**

$P(C_{hot}) = 0.2/100$

$P(A<->B, C_{hot}) = P(A<->B|C_{hot})P(C_{hot})$

$\frac{0.01}{100} = \frac{0.45}{100}\frac{2}{100}$

**What can we get out of Fig1?**

The displacement of inter-landscape transition hotspots (magenta in Fig. 1(B)) from minimum energy regions on both landscapes highlights the need for significant conformational changes before and after transition between the two energy landscapes

At the same time, the presence of several inter-landscape transition hotspots indicates a multiplicity of routes to ligand binding with comparable transition probabilities

**What does Fig2 shows?**

The graph shows the overall conformational evolution at selected points along the ligand-binding route shown in Fig. 1(A)

For a 28-frame 3D movie of the conformational changes along this representative route, see Movies 1-4

**What does the graphs tell us?**

Our results shed light on the longstanding question regarding the “population shift” vs. “induced fit” models.

“population shift” [3] requires a conformational change before ligand binding
“induced fit” [2] requires a conformational change after ligand binding

**What is the binding mechanism, Induced fit or populations fit?**

We find that ligand binding, at least in RyR1, proceeds via a continuum of conformations increasingly different from, and at higher energies than the minimum-energy conformation on the –ligand landscape. These higher-energy conformations are reached thermally via “population shift”

Collision with a ligand then transfers RyR1 to the +ligand energy landscape, where a downward slope in energy drives further continuous conformational changes to the minimum-energy, ligand-bound state. The conformational changes after collision with a ligand constitute an “induced shift”

Our results thus show that each of the two opposing models describes a different part of the actual process; at least for RyR1, binding entails specific conformational changes both before and after collision with a ligand.

**Q: What are the proportions of populations shift vs induced fit?**

The exact apportionment of the conformational changes before and after collision depends, of course, on the point at which the transition to the +ligand landscapes takes place. Although transitions can occur over a relatively broad region, the highest probabilities are concentrated at a few “hotspots” (Fig. 1(B)). The positions of the ±ligand energy minima relative to the broad region of significant transition probability indicate that most ligand-binding events in RyR1 involve a greater element of “population shift” than “induced fit”, as suggested in [22], but this could be system specific.

**Q: What does the results say about binding site's configurations**

Our results also provide new insights into structural changes at specific binding sites (Fig. 3), and in the pore region (Fig. 4)

A full discussion of these results and their implications for the gating mechanism of RyR1 is beyond the scope of the present paper

Here, we summarize a few salient points.

1. the readily accessible conformations on the −ligand and +ligand energy landscapes overlap significantly. This indicates that, even in the absence of activating ligands (Ca2+, ATP and caffeine), RyR1 is able to assume, albeit with low probability, conformations previously thought to require the presence of a bound ligand [12, 23]

2. moving along the ligand-binding conduit, we observe a motion of the cytoplasmic domain correlated with activation of the core and opening of the channel. The wing movements actuate a complex set of motions in the activation domain, which in turn bends the pore helix responsible for the opening of the pore
3. the atomic coordinates of RyR1 obtained by modeling along the binding trajectory reveal the conformational changes at the binding sites of the ligands (Ca2+, ATP and caffeine) in the course of ligand binding,

* Starting at the minimum-energy point of the –ligand landscape, the Ca2+ binding site gradually contracts until the transition to the ligand-bound landscape is effected, after which the binding-site conformation stabilizes in its ligand-bound state (Fig. 3)

*  the ATP binding site does not display a large distance change along the entire conduit connecting the energy minima of the ±ligand landscapes (Fig. 3). This binding site thus seems to require little, if any accommodation to bind its ligand, indicating the ATP-bound conformation of the domain is close to the minimum energy conformation, whether ATP is bound or not. ATP’s main role appears to involve rigidifying the pore-helix/C-terminal domain hinge, thus performing a structural role necessary for gating.

* They dont talk about caffeine???

**What are the authors suggesting as future tasks?**

Clearly, equilibrium measurements cannot answer all questions regarding dynamics. It would thus be illuminating to compare the present results with those obtained from observation of non-equilibrium ensembles engaged in reaction. Also, using larger equilibrium datasets, it would be interesting to investigate the role of conformations lying at higher energies. These constitute future tasks.

# References

**Thermal equilibrium with ligand and thermal bath**
[12] 

**How to combine the cryo-EM snapshots**
[15]
[16]
[17]

**Fermi's Golden Rule to find the transitions probablities between two landscapes**
[18]
[19]

**Grouping the snapshots into uniformly spaced orientational classes**
[20]

**How to find the reaction coordinates from the pooled snapshots**
[17,21]