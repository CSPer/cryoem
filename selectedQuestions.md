**Q:What did they do?**

They present 

* quantitative free energy landscapes,
* reaction coordinates,
* and 3D movies
  
of the continous conformational changes associated with the binding of activating ligand.

before and after the introduction of ligands

**Q: What did they use?**

They used snapshots of RyR1.

**Q: What did they use to take the snapshots of RyR1?**

They used single-particle cryo-EM

**Q: What is RyR1?**

It is a Ca 2+ channel involved in skeletal muscle excitation/contraction coupling.

**Q: What did they find?**

They found multiple routes to ligand binding with comparable branching ratios

They found that all high-probability routes involve significant conformational changes before and after the binding of ligands

They found new insights about the local structural changes along the ligand binding route including the accomodations at the

* calcium  
* ATP
* caffeine

binding sites

**Q: What does Fig1 show?**

1A: shows the ±ligand energy landscapes of RyR1

Assuming the probability of a collision (not binding) with a ligand is independent of conformation, the probability of a transition between equivalent points on the two landscapes can be estimated from Fermi’s Golden Rule for the period immediately after the exposure of RyR1 molecules to the reservoir containing ligands

$P(Collision|C) = P(Collision)$

$P(A<->B|C)$ : Transition probablity between bound and unbount state given a configuration

$P(A<->B, C) = P(A<->B|C)P(C)$

$P(A<->B, C)$ : probablity of transition between equivalent points on the two landscapes.

$A$ : Ligand not bound

$B$ : Ligand bound state

$C$ : The configuration of molecule

$<->$ : transitions between two states in both ways



1B: Inter-landscape transition probablity

the inter- landscape transition probability displays specific “hotspot” regions, where a significant number of ligand-free and ligand-bound macromolecules display the same conformation

Does this mean that the bound configurations also exist in the unbound configurations and has a high transtions probablity. Thus it indicates of a selective binding mechanism rather than an induced fit.

The mechanism:

The most probable routes to ligand binding start from the region of lowest energy on the –ligand landscape (“START” in Fig. 1(A)), reach one of the hotspot transition points (“HOT”) with a probability of ~2%, cross to the +ligand landscape also with ~ 0.45% of the probability of a collision with a ligand, and terminate in the region of lowest energy on the +ligand landscape (“FINISH”). This means ~ 0.01% of collisions with a ligand lead to binding



**most probable route:**

$P(C_{hot}) = 0.2/100$

$P(A<->B, C_{hot}) = P(A<->B|C_{hot})P(C_{hot})$

$\frac{0.01}{100} = \frac{0.45}{100}\frac{2}{100}$

**What can we get out of Fig1?**

The displacement of inter-landscape transition hotspots (magenta in Fig. 1(B)) from minimum energy regions on both landscapes highlights the need for significant conformational changes before and after transition between the two energy landscapes

At the same time, the presence of several inter-landscape transition hotspots indicates a multiplicity of routes to ligand binding with comparable transition probabilities

**What does Fig2 shows?**

The graph shows the overall conformational evolution at selected points along the ligand-binding route shown in Fig. 1(A)

For a 28-frame 3D movie of the conformational changes along this representative route, see Movies 1-4

**What does the graphs tell us?**

Our results shed light on the longstanding question regarding the “population shift” vs. “induced fit” models.

“population shift” [3] requires a conformational change before ligand binding
“induced fit” [2] requires a conformational change after ligand binding

**What is the binding mechanism, Induced fit or populations fit?**

Each of the two opposing models describes a different part of the actual process; at least for RyR1, binding entails specific conformational changes both before and after collision with a ligand.

We find that ligand binding, at least in RyR1, proceeds via a continuum of conformations increasingly different from, and at higher energies than the minimum-energy conformation on the –ligand landscape. These higher-energy conformations are reached thermally via “population shift”

Collision with a ligand then transfers RyR1 to the +ligand energy landscape, where a downward slope in energy drives further continuous conformational changes to the minimum-energy, ligand-bound state. The conformational changes after collision with a ligand constitute an “induced shift”

**Q: What are the proportions of populations shift vs induced fit?**

The exact apportionment of the conformational changes before and after collision depends, of course, on the point at which the transition to the +ligand landscapes takes place. Although transitions can occur over a relatively broad region, the highest probabilities are concentrated at a few “hotspots” (Fig. 1(B)). The positions of the ±ligand energy minima relative to the broad region of significant transition probability indicate that most ligand-binding events in RyR1 involve a greater element of “population shift” than “induced fit”, as suggested in [22], but this could be system specific.

**Q: What does the results say about binding site's configurations**
* CA+ binding site closes and stabilizes after ligan is bound
* ATP binding site does not change much
* CAffeine they dont speak about them

Here, we summarize a few salient points.

1. the readily accessible conformations on the −ligand and +ligand energy landscapes overlap significantly. This indicates that, even in the absence of activating ligands (Ca2+, ATP and caffeine), RyR1 is able to assume, albeit with low probability, conformations previously thought to require the presence of a bound ligand [12, 23]

2. moving along the ligand-binding conduit, we observe a motion of the cytoplasmic domain correlated with activation of the core and opening of the channel. The wing movements actuate a complex set of motions in the activation domain, which in turn bends the pore helix responsible for the opening of the pore
3. the atomic coordinates of RyR1 obtained by modeling along the binding trajectory reveal the conformational changes at the binding sites of the ligands (Ca2+, ATP and caffeine) in the course of ligand binding,

* Starting at the minimum-energy point of the –ligand landscape, the Ca2+ binding site gradually contracts until the transition to the ligand-bound landscape is effected, after which the binding-site conformation stabilizes in its ligand-bound state (Fig. 3)

*  the ATP binding site does not display a large distance change along the entire conduit connecting the energy minima of the ±ligand landscapes (Fig. 3). This binding site thus seems to require little, if any accommodation to bind its ligand, indicating the ATP-bound conformation of the domain is close to the minimum energy conformation, whether ATP is bound or not. ATP’s main role appears to involve rigidifying the pore-helix/C-terminal domain hinge, thus performing a structural role necessary for gating.

* They dont talk about caffeine???